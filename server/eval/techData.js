var GoogleSpreadsheet = require('google-spreadsheet');
var async = require('async');

var doc = new GoogleSpreadsheet('13kfzVd32dvDsmU0omV2CBCZ5wr32yMQ4LNrfuvlYcso');
var sheet;

async.series({
	auth: step => {
		var creds = require('./google-generated-creds.json');
		var creds_json = {
    		client_email: 'yourserviceaccountemailhere@google.com',
    		private_key: 'your long private key stuff here'
    	}

    	doc.useServiceAccountAuth(creds, step);
	}
});