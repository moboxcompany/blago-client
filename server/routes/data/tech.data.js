var _		   = require('underscore');

module.exports = {
	getTechTypes: getTechTypes,
	getModels: findModelsByTypeId
}

function getTechTypes(){
	return _.map(typesState, (val, key) => {
		return { id: val.id, name: key };
	});
}

function findModelsByTypeId(typeId) {
	return _.propertyOf( _.filter(typesState, (val, key) => {
		return val.id == typeId;
	})[0])('models');
}

var typesState = {
			"Смартфоны": {
				id: 15,
				0: {
					min: 63,
					max: 73
				},
				1: {
					min: 63,
					max: 73
				},
				2: {
					min: 67,
					max: 77
				},
				3: {
					min: 71,
					max: 61
				},
				4: {
					min: 75,
					max: 85
				},
				models: [
					{
						brand: 'Мajestiс',
						model: 'TAB 178 3G',
						price: '480'
					},
					{
						brand: 'Acer',
						model: 'TAB 178 3G',
						price: '480'
					},
					{
						brand: 'Acer',
						model: 'Z200',
						price: '280'
					},
					{
						brand: 'Acer',
						model: 'Liquid E Ferrari',
						price: '710'
					},
					{
						brand: 'Acer',
						model: 'Liquid S1',
						price: '120'
					},
					{
						brand: 'Acer',
						model: 'S520 (Liquid S2)',
						price: '120'
					},
					{
						brand: 'Acer',
						model: 'Z520',
						price: '789'
					},
					{
						brand: 'Acer',
						model: 'Acer Z500',
						price: '3527'
					},
					{
						brand: 'Acer',
						model: 'Liquid E1',
						price: '789'
					}
				]
			},
			"Планшеты": {
				id: 14,
				0: {
					min: 63,
					max: 73
				},
				1: {
					min: 63,
					max: 73
				},
				2: {
					min: 67,
					max: 77
				},
				3: {
					min: 71,
					max: 61
				},
				4: {
					min: 75,
					max: 85
				},
				models: [
					{
						brand: 'Acer',
						model: 'Liquid E1',
						price: '789'
					},
					{
						brand: 'WinBook',
						model: 'TW800 2',
						price: '100,00'
					},
					{
						brand: 'Мajestiс',
						model: 'TAB 178 3G',
						price: '480'
					},
					{
						brand: 'Acer',
						model: 'TAB 178 3G',
						price: '480'
					},
					{
						brand: 'Acer',
						model: 'Z200',
						price: '280'
					},
				]
			},
			"Фотаппараты": {
				id: 18,
				0: {
					min: 63,
					max: 73
				},
				1: {
					min: 63,
					max: 73
				},
				2: {
					min: 67,
					max: 77
				},
				3: {
					min: 71,
					max: 61
				},
				4: {
					min: 75,
					max: 85
				},
				models: [
					{
						brand: 'Мajestiс',
						model: 'TAB 178 3G',
						price: '480'
					},
					{
						brand: 'Acer',
						model: 'TAB 178 3G',
						price: '480'
					},
					{
						brand: 'Acer',
						model: 'Z200',
						price: '280'
					}
				]
			},
			"Видеокамеры": {
				id: 1,
				0: {
					min: 63,
					max: 73
				},
				1: {
					min: 63,
					max: 73
				},
				2: {
					min: 67,
					max: 77
				},
				3: {
					min: 71,
					max: 61
				},
				4: {
					min: 75,
					max: 85
				},
				models: [
					{
						brand: 'Мajestiс',
						model: 'TAB 178 3G',
						price: '480'
					},
					{
						brand: 'Acer',
						model: 'TAB 178 3G',
						price: '480'
					},
					{
						brand: 'Acer',
						model: 'Z200',
						price: '280'
					}
				]
			},
			"Телевизоры": {
				id: 42,
				0: {
					min: 66,
					max: 76
				},
				1: {
					min: 66,
					max: 76
				},
				2: {
					min: 70,
					max: 80
				},
				3: {
					min: 74,
					max: 84
				},
				4: {
					min: 78,
					max: 85
				},
				models: [
					{
						brand: 'Мajestiс',
						model: 'TAB 178 3G',
						price: '480'
					},
					{
						brand: 'Acer',
						model: 'TAB 178 3G',
						price: '480'
					},
					{
						brand: 'Acer',
						model: 'Z200',
						price: '280'
					}
				]
			},
			"GPS-навигатор": {
				id: 33,
				0: {
					min: 43,
					max: 53
				},
				1: {
					min: 43,
					max: 53
				},
				2: {
					min: 47,
					max: 57
				},
				3: {
					min: 51,
					max: 61
				},
				4: {
					min: 55,
					max: 65
				},
				models: [
					{
						brand: 'Мajestiс',
						model: 'TAB 178 3G',
						price: '480'
					},
					{
						brand: 'Acer',
						model: 'TAB 178 3G',
						price: '480'
					},
					{
						brand: 'Acer',
						model: 'Z200',
						price: '280'
					}
				]
			},
			"HDD-накопитель (внешний жесткий диск)": {
				id: 34,
				0: {
					min: 43,
					max: 53
				},
				1: {
					min: 43,
					max: 53
				},
				2: {
					min: 47,
					max: 57
				},
				3: {
					min: 51,
					max: 61
				},
				4: {
					min: 55,
					max: 65
				},
				models: [
					{
						brand: 'Мajestiс',
						model: 'TAB 178 3G',
						price: '480'
					},
					{
						brand: 'Acer',
						model: 'TAB 178 3G',
						price: '480'
					},
					{
						brand: 'Acer',
						model: 'Z200',
						price: '280'
					}
				]
			},
			"MP-3 плеер Apple Ipod": {
				id: 35,
				0: {
					min: 43,
					max: 53
				},
				1: {
					min: 43,
					max: 53
				},
				2: {
					min: 47,
					max: 57
				},
				3: {
					min: 51,
					max: 61
				},
				4: {
					min: 55,
					max: 65
				},
				models: [
					{
						brand: 'Мajestiс',
						model: 'TAB 178 3G',
						price: '480'
					},
					{
						brand: 'Acer',
						model: 'TAB 178 3G',
						price: '480'
					},
					{
						brand: 'Acer',
						model: 'Z200',
						price: '280'
					}
				]
			},
			"Видеорегистраторы": {
				id: 2,
				0: {
					min: 43,
					max: 53
				},
				1: {
					min: 43,
					max: 53
				},
				2: {
					min: 47,
					max: 57
				},
				3: {
					min: 51,
					max: 61
				},
				4: {
					min: 55,
					max: 65
				},
				models: [
					{
						brand: 'Мajestiс',
						model: 'TAB 178 3G',
						price: '480'
					},
					{
						brand: 'Acer',
						model: 'TAB 178 3G',
						price: '480'
					},
					{
						brand: 'Acer',
						model: 'Z200',
						price: '280'
					}
				]
			},
			"Игровые приставки": {
				id: 38,
				0: {
					min: 43,
					max: 53
				},
				1: {
					min: 43,
					max: 53
				},
				2: {
					min: 47,
					max: 57
				},
				3: {
					min: 51,
					max: 61
				},
				4: {
					min: 55,
					max: 65
				}
			},
			"Мониторы" : {
				id: 40,
				0: {
					min: 43,
					max: 53
				},
				1: {
					min: 43,
					max: 53
				},
				2: {
					min: 47,
					max: 57
				},
				3: {
					min: 51,
					max: 61
				},
				4: {
					min: 55,
					max: 65
				}
			},
			"Электронная книга": {
				id: 32,
				0: {
					min: 43,
					max: 53
				},
				1: {
					min: 43,
					max: 53
				},
				2: {
					min: 47,
					max: 57
				},
				3: {
					min: 51,
					max: 61
				},
				4: {
					min: 55,
					max: 65
				}
			},
			"Мелкобытовая техника": {
				id: 39,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Мелкобытовая техника (Блендеры)": {
				id: 4,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Мелкобытовая техника (Кухонные комбайны)": {
				id: 5,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Мелкобытовая техника (Микроволновая печь)": {
				id: 6,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Мелкобытовая техника (Мультиварки)": {
				id: 7,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Мелкобытовая техника (Мясорубки)": {
				id: 8,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Мелкобытовая техника (Пароварки)": {
				id: 9,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Мелкобытовая техника (Пылесосы)": {
				id: 10,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Мелкобытовая техника (Соковыжималки)": {
				id: 11,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Мелкобытовая техника (Утюги)": {
				id: 37,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Мелкобытовая техника (Хлебопечки)": {
				id: 12,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Мелкобытовая техника (Чайники)": {
				id: 13,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Электроинструмент": {
				id: 43,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Электроинструмент (Дрели и шуруповерты)": {
				id: 20,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Электроинструмент (Дрели ударные)": {
				id: 21,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Электроинструмент (Лазерный  инструмент)": {
				id: 22,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Электроинструмент (Лобзики электрические)": {
				id: 23,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Электроинструмент (Перфораторы)": {
				id: 24,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Электроинструмент (Пилы электрические, бензиновые)": {
				id: 25,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Электроинструмент (Рубанки электрические)": {
				id: 26,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Электроинструмент (Угловая шлифмашина (Болгарка))": {
				id: 27,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Электроинструмент (Фен строительный)": {
				id: 28,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Электроинструмент (Фрезеры)": {
				id: 29,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Электроинструмент (Шлифовальные машины)": {
				id: 30,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			},
			"Электроинструмент (Шуруповерты аккумуляторные)": {
				id: 31,
				0: {
					min: 39,
					max: 49
				},
				1: {
					min: 39,
					max: 49
				},
				2: {
					min: 41,
					max: 51
				},
				3: {
					min: 45,
					max: 55
				},
				4: {
					min: 49,
					max: 59
				}
			}
		};