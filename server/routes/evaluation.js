router.get('/evaluation', (req, res) => {
	var options = {
		root: __dirname + '/../../dist/',
		dotfiles: 'deny',
		headers: {
		    'x-timestamp': Date.now(),
		    'x-sent': true
		}
	};
	
    var fileName = 'evaluation.html';
	res.sendFile(fileName, options, err => {
		if( err ){
			console.log(err);
		}
	});
});

var techType = require('./data/tech.data.js');

router.get('/evaluation/techType', (req, res) => {
	var tType = techType.getTechTypes();
	res.send(tType);
});

router.get('/evaluation/techType/:typeId', (req, res) => {
	var typeId = req.params.typeId;
	var models = techType.getModels(typeId);

	res.json(models);
});