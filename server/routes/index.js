'use strict';

global.express = require('express');
global.router  = express.Router();

var endpoints  = [
	'./evaluation',
	'./anketa',
	'./opros',
	'./quest'
];

endpoints.forEach(function(endpoint){
	require(endpoint);
});

module.exports = router;