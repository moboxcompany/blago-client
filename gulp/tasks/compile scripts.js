'use strict';

var jshint          = require('gulp-jshint');
var seq             = require('sequence-stream');
var concat          = require('gulp-concat');
var templateCache   = require('gulp-angular-templatecache');
var sourcemaps      = require('gulp-sourcemaps');
var babel           = require('gulp-babel');

gulp.task('yii2-scripts', function(){

    var libs    = gulp.src(config.paths.src.vendors.libs, {base: 'vendors'});

    var vendors = gulp.src(config.paths.src.vendors.scripts, {base: 'vendors'});

    var app = gulp.src(config.paths.src.components , {base: 'src'});

    var views = gulp.src([
        './client/src/**/*.html'
    ])
        .pipe(templateCache({
            module: 'app'
        }));

    // Combine all the streams
    return seq([libs, vendors, app, views])
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(concat('app.js'))
        .pipe(sourcemaps.write('maps', {sourceRoot: '/client'}))
        .pipe(gulp.dest('../js'))
        .on('end', browserSync.reload);
});