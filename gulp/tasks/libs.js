'use strict';

var print = require('gulp-print');

gulp.task('libs', function(){
    return gulp.src([
        	'node_modules/systemjs/dist/system.js',
        	'node_modules/babel-polyfill/dist/polyfill.js'
        ])
        .pipe(print())
        .pipe(gulp.dest('dist/libs'));
});