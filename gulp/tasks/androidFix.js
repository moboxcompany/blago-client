'use strict';

var sass            = require('gulp-sass');
var concat          = require('gulp-concat');
var seq             = require('sequence-stream');
var cssGlobbing     = require('gulp-css-globbing');
var sourcemaps      = require('gulp-sourcemaps');
var autoprefixer    = require('gulp-autoprefixer');

gulp.task('android:fix', function(){

    var compile = gulp.src(['./client/scss/libs/android.scss'], {base: 'app'})
        .pipe(cssGlobbing({
            extensions : ['.scss']
        }))
        .pipe(sass());

    // Combine all the streams
    return compile
        .pipe(autoprefixer())
        .pipe(concat('android.css'))
        .pipe(gulp.dest('dist'));

});
