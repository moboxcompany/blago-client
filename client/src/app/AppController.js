(function(){
    'use strict';

    angular
        .module('app')
        .controller('AppController', AppController);

    function AppController($state, $window, $location){

        var vm = this;

        angular.extend(vm, {
            title: 'Благо'
        });
        
    }

})();