(() => {

	angular
		.module('app')
		.controller('MapCtrl', mapController)

	function mapController($scope, NgMap, NavigatorGeolocation, $uibModal, spinnerService, api, $log, $window) {

		var vm = this;

		vm.mapInitiation 	= false;

		vm.showSearchTitle 	= false;

		vm.googleMapsUrl 	= 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDEkVB4nZR44FqQYuvVo88Xgp6-ASfycSo';

		vm.region  			= {
			available: [],
			selected: { id: 1, name: 'Регион' }
		};

		vm.city	  			= {
			available: [],
			selected: { id: 1, name: 'Город' }
		};

		vm.markers 			= [];

		vm.resetMarkers		= resetMarkers;

		vm.markerCluster;

		vm.mapItems		= [];

		vm.findCities = () => {
			setCity(vm.region.selected.id);
		};

		vm.changeMarkers = initLocation;

		vm.getOffices 	= setDepartments;

		$scope.mapItemsPage		= 1;

		$scope.mapItemsPerPage	= 10;

		$scope.mapItemsMaxSize  = 3;

		vm.openModal = openModalFn;

		vm.locationPermission = '';

		vm.activeMap = false;

		let icon,
			mcOptions;

		vm.loadMap = () => {
			vm.mapInitiation = true;
		}

		setRegion();
		detectUserLocation();

		function setMap() {
			console.log('permission denied, map iniation start');
			vm.loadMap();

			spinnerService.show('mapSpinner');

			return getMap(getMarkers).then(data => {
			}).finally(() => {
				spinnerService.hide('mapSpinner');
			});
		};

		function initLocation() {
			if( vm.locationPermission ) {
				goToPage('nearest-department');
			} else {
				setMap();
			}
		};

		function detectUserLocation() {
			console.log('start detecting user location');
			NavigatorGeolocation.getCurrentPosition()
				.then(data => {
					vm.locationPermission = true;
					console.log(vm.locationPermission);
				}, () => {
					vm.locationPermission = false;
					console.log(vm.locationPermission);
				});
		};

		function goToPage(page) {
			console.log('permission granted, change page to: ', page);
			$window.location.pathname = '/' + page;
		};

		function getMap(doneFn) {
			return NgMap.getMap()
				.then(doneFn)
				.catch(error => {
					$log.info('ny pizda');
					$log.error(error);
				});
		};

		function getMarkers(map) {
			return api.getMarkers(vm.city.selected.id)
				.then(markersData => {

					vm.resetMarkers();

					if( markersData ) {
						vm.markers = markersData.map(mark => {
							let position = 	new google.maps.LatLng(mark.lat, mark.long);

							let marker 	 = new google.maps.Marker({
								position: position,
								map: map,
								icon: icon,
								title: 'Blago',
								zIndex: 5
							});

							marker.addListener('click', () => {
								vm.openModal(mark);
							});

							vm.mapItems.push({
								officeNumber: mark.id,
								text: mark.address + '<br/>' + mark.work + '<br/>' + mark.description
							});

							marker.setMap(map);

							return marker;
						});

						if( vm.markerCluster )
							vm.markerCluster.setMap(null);
						vm.markerCluster = new MarkerClusterer(map, vm.markers, mcOptions);
						vm.markerCluster.setMaxZoom(9);
					}
					return vm.markers;
				});
		};

		// Todo: markers deletion
		function resetMarkers() {
			$log.info(vm.markers);
			$log.info('reseting markers');
			for (var i = 0; i < vm.markers.length; i++) {
				vm.markers[i].setMap(null);
			}
			vm.markers = [];
			vm.mapItems = [];
			$log.info('markers delete');
			$log.info(vm.markers);
		}

		function setCity(regionId) {
			return getCities(regionId).then(data => {
					// $log.info(data)
				});
		};

		function getCities(regionId) {
			return api.getCities(regionId)
				.then(data => {
					vm.city.available = data;
					return vm.city;
				});
		};

		function setRegion() {
			return getRegions().then(data => {
					// $log.info(data)
				});
		};

		function getRegions() {
			return api.getRegions()
				.then(data => {
					vm.region.available = data;
					return vm.region;
				});
		};

		function setDepartments() {
			return getMap(getDepartments).then(data => {
					// $log.info(data)
				});
		};

		function getDepartments(map) {
			return api.getDepartments().then(data => {

					icon = {
						url: 'img/map_icon.png',
						size: new google.maps.Size(91, 55),
						origin: new google.maps.Point(0,0),
						anchor: new google.maps.Point(0,32)
					};

					mcOptions = {
						styles: [
							{
								height: 53,
								url: "http://blago.ua/themes/classic/img/google_cluster/m1.png",
								width: 53
							},
							{
								height: 56,
								url: "http://blago.ua/themes/classic/img/google_cluster/m2.png",
								width: 56
							},
							{
								height: 66,
								url: "http://blago.ua/themes/classic/img/google_cluster/m3.png",
								width: 66
							},
							{
								height: 78,
								url: "http://blago.ua/themes/classic/img/google_cluster/m4.png",
								width: 78
							},
							{
								height: 90,
								url: "http://blago.ua/themes/classic/img/google_cluster/m5.png",
								width: 90
							}
						]
					};

					vm.resetMarkers();

					vm.markers = data.map(mark => {
						let position = 	new google.maps.LatLng(mark.lat, mark.long);

						let marker 	 = new google.maps.Marker({
							position: position,
							map: map,
							icon: icon,
							title: 'Blago',
							zIndex: 5
						});

						marker.addListener('click', () => {
							vm.openModal(mark);
						});

						vm.mapItems.push({
							officeNumber: mark.id,
							cords: {
								lat: mark.lat,
								lng: mark.long
							},
							text: mark.address + '<br/>' + mark.work + '<br/>' + mark.description
						});

						marker.setMap(map);

						return marker;
					});

					if( vm.markerCluster )
						vm.markerCluster.setMap(null);
					vm.markerCluster = new MarkerClusterer(map, vm.markers, mcOptions);
					vm.markerCluster.setMaxZoom(9);

					return map;
				});
		};

		function openModalFn(opt) {
			let modal = $uibModal.open({
				templateUrl: 'templates/map.modal.html',
				resolve: {
					options: () => {
						return opt;
					}
				},
				controller: ModalInstanceCtrl
			});

			modal.result.then(depId => {
				calcDestinationPath(depId);
			}, cls => {
				$log.info('Modal dismissed');
			});
		}

		function ModalInstanceCtrl($scope, $uibModalInstance, options){
			// Mock image, real image - options.image
			let image = 'img/blago_punkt.jpg';
			$scope.image		= '<img ng-src="'+image+'">';
			$scope.text 		= options.address + '<br/>' + options.work + '<br/>' + options.description;
			$scope.officeNumber = options.id;

			$scope.calcPath = () => {
				$uibModalInstance.close($scope.officeNumber);
			};

			$scope.close = () => {
				$uibModalInstance.dismiss({
					$value: 'cancel'
				});
			};
		};

		function calcDestinationPath(depId) {

		}

	};

})();