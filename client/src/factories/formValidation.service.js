(() => {

	angular
		.module('app')
		.factory('formValidation', formValidation);

	//formValidation.$inject['_'];
	//TODO: include underscore

	function formValidation(){

		var vm = this;

		var service = {
			checkErr: checkErrorFields,
			checkErrExpt: checkErrorFieldsExceptNames,
			checkErrWhr: checkErrorFieldsWhereNames,
			getErr: getErrorFields,
			getErrWhr: getErrorFieldsWhereNames,
			setErr: setErrorToFields,
			setScope: setScope,
			setForm: setForm
		};

		return service;

		function checkErrorFields(){
			return vm.scope[vm.form].$error.required === undefined ? true : (vm.scope[vm.form].$error.required.length == 0 ? true : false);
		}

		function checkErrorFieldsExceptNames(names){
			var errs = vm.scope[vm.form].$error.required;
			if( errs === undefined ) return true;
			var newErr = errs.filter(err => {
				return names.indexOf(err.$name) > -1 ? false : true;
			});
			return newErr.length == 0 ? true : false;
		}

		function checkErrorFieldsWhereNames(names){
			var arr = vm.scope[vm.form].$error.required.filter(err => {
				return names.indexOf(err.$name) > -1 ? true : false;
			});
			return arr === undefined ? true : (arr == 0 ? true : false);
		}

		function getErrorFields(){
			return vm.scope[vm.form].$error.required
						.concat(vm.scope[vm.form].$error.isSimilar)
						.filter(item => {
							return typeof item === 'object';
						});
		}

		function getErrorFieldsWhereNames(names){
			return getErrorFields().filter(err => {
				return names.indexOf(err.$name) > -1 ? true : false;
			});
		}

		function setErrorToFields(fields){
			return fields.forEach(field => {
				if( field !== undefined )
					field.$setDirty();
			});
		}

		function setScope($scope){
			vm.scope = $scope;
		}

		function setForm($form){
			vm.form  = $form;
		}

	}

})();