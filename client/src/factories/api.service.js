(() => {

    angular
        .module('app')
        .factory('api', apiProvider);

    function apiProvider($http, $log) {

        var server = 'https://mobile.blago.local/api/web/v1';

        var service = {
            getTelephoneNumber: getTelephoneNumber,
            getSlides: getSlides,
            getHomeBlocks: getHomeBlocks,
            getRegions: getRegions,
            getCities: getCities,
            getMarkers: getMarkers,
            getDepartments: getAllDepartments,
            getNearestDepartment: getNearestDepartment
        };

        return service;
        
        function getTelephoneNumber() {
            return $http.get(server+'/home/get-number')
                        .then(getTelNumComplete)
                        .catch(getTelNumFailed);

            function getTelNumComplete(response) {
                return response.data;
            }

            function getTelNumFailed(error) {
                $log.info(error);
            }
        }

        function getSlides() {
            return $http.get(server+'/home/get-slides')
                        .then(getSlidesComplete)
                        .catch(getSlidesFailed);

            function getSlidesComplete(response) {
                return response.data;
            }

            function getSlidesFailed(error) {
                $log.info(error);
            }
        }

        function getHomeBlocks() {
            return $http.get(server+'/home/blocks')
                        .then(getHomeBlocksComplete)
                        .catch(getHomeBlocksFailed);

            function getHomeBlocksComplete(response) {
                return response.data;
            }

            function getHomeBlocksFailed(error) {
                $log.info(error);
            }
        }

        function getRegions() {
            return $http.get(server + '/office-address/regions')
                        .then(getRegionsComplete)
                        .catch(getRegionsFailed);

            function getRegionsComplete(response) {
                return response.data;
            }

            function getRegionsFailed(error) {
                $log.info(error);
            }
        }

        function getCities(regionId) {
            return $http.get(server + '/office-address/cities/'+regionId)
                        .then(getCitiesComplete)
                        .catch(getCitiesFailed);

            function getCitiesComplete(response) {
                return response.data;
            }

            function getCitiesFailed(error) {
                $log.info(error);
            }
        }

        function getMarkers(cityId) {
            return $http.get(server + '/office-address/offices/'+cityId)
                        .then(getMarkersComplete)
                        .catch(getMarkersFailed);

            function getMarkersComplete(response) {
                return response.data;
            }

            function getMarkersFailed(error) {
                $log.info(error);
            }
        }

        function getAllDepartments() {
            return $http.get(server + '/office-address/offices')
                        .then(getAllDepartmentsComplete)
                        .catch(getAllDepartmentsFailed);

            function getAllDepartmentsComplete(response) {
                return response.data;
            }

            function getAllDepartmentsFailed(error) {
                $log.info(error);
            }
        }

        function getNearestDepartment(coords) {
            let data = $.param({
                lat: coords.lat,
                lng: coords.lng
            }),
                config = {
                    headers : {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                };
            return $http.post(server + '/office-address/nearest', data, config)
                        .then(getNearDepComplete)
                        .catch(getNearDepFailed);

            function getNearDepComplete(response) {
                return response.data;
            }

            function getNearDepFailed(error) {
                $log.info(error);
            }
        }

    };

})();