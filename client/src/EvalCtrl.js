(() => {

	angular
		.module('app')
		.controller('EvaluationCtrl', EvalCtrl);

	EvalCtrl.$inject = ['$scope', '$uibModal', 'formHttp', 'formValidation', 'client', '$timeout'];

	function EvalCtrl($scope, $uibModal, formHttp, formValidation, client, $timeout) {
		const vm = this;

		/* Set up formValidation service
		 * TODO: configure ctrl to avoid setting scope !
		 * BAD PRACTICE !!!
		 * Руки оторвать себе хочу за это
		 */
		formValidation.setScope($scope);
		formValidation.setForm('evalForm');

		vm.externalStateIcons = client.exStIco();

		vm.metalType = ['Золото', 'Серебро'];
		vm.goldType = {
			375: 378,
			500: 504,
			583: 588,
			585: 590,
			750: 756,
			850: 857,
			900: 907
		};
		vm.silverType = {
			875: 12.5,
			925: 13
		};
		vm.goldPrice = 0;
		vm.silverPrice = 0;
		vm.price = 0;
		vm.techPrice = 0;
		vm.handCasheGold   = 0;
		vm.handCasheSilver = 0;
		vm.handCashe = 0;
		vm.monthDays = Array.from({length: 29}, (v, k) => k+1);
		vm.metalProbe = vm.goldType;
		vm.searchOffice = false;
		vm.userInfo = true;
		vm.techErrState = true;
		vm.techErr = [];
		vm.techTypes = [];
		vm.techBrands = [{
			id: 0,
			name: 'Выберите бренд'
		}];
		vm.techModels = [{
			id: 0,
			name: 'Выберите модель',
			price: 0
		}];

		$scope.evaluation = {};
		$scope.evaluation.per = {};
		$scope.evaluation.cash = 0;
		$scope.evaluation.cash = 0;
		$scope.evaluation.metal = {};
		$scope.evaluation.percDays = vm.monthDays[4];
		$scope.evaluation.metal.type = vm.metalType[0];
		$scope.evaluation.metal.probe = vm.metalProbe[375];
		$scope.evaluation.techType = vm.techTypes[0];
		$scope.evaluation.techBrand = vm.techBrands[0];
		$scope.evaluation.techModel = vm.techModels[0];

		/* Gold calculator */
		/*vm.calculateGold = () => {
			var g = $scope.evaluation.gold;
			if( formValidation.checkErrWhr(['goldType', 'goldWeight']) ){
				vm.goldPrice = vm.goldType[g.goldType] * g.goldWeight;
			} else {
				var err = formValidation.getErrWhr(['goldType', 'goldWeight']);
				formValidation.setErr(err);
			}
		};*/

		vm.calculate = () => {
			if( formValidation.checkErrWhr(['metalType', 'metalWeight', 'metalProbe']) ){
				var w = parseFloat($scope.evaluation.metal.weight);
				var p = parseFloat($scope.evaluation.metal.probe);
				if( detectMetal() == 'gold' ) {
					vm.price = p * w;
					vm.handCashe = calcHandCashe('gold', vm.price);
				}
				if( detectMetal() == 'silver' ) {
					vm.price = p * w;
					vm.handCashe = calcHandCashe('silver', vm.price);
				}
			} else {
				var err = formValidation.getErrWhr(['metalType', 'metalWeight', 'metalProbe']);
				formValidation.setErr(err);
			}
		};

		function detectMetal() {
			if( $scope.evaluation.metal.type == 'Золото' )
				return 'gold';
			if( $scope.evaluation.metal.type == 'Серебро' )
				return 'silver';
		}

		function calcHandCashe(type, cash) {
			if( type = 'gold' ) {
				if( cash < 5000 ){
					return cash * 0.066;
				}
				if( cash >= 5000 && cash < 10000 ){
					return cash * 0.063;
				}
				if( cash >= 10000 ){
					return cash * 0.061;
				}
			}
			if( type = 'silver' ) {
				return cash * 0.158;
			}
		}

		/* Silver calculator */
		vm.selectMetal = type => {
			if( type == "Золото" ) {
				vm.metalProbe = vm.goldType;
				$scope.evaluation.metal.probe = vm.metalProbe[375];
			} else {
				vm.metalProbe = vm.silverType;
				$scope.evaluation.metal.probe = vm.metalProbe[875];
			}
		};

		/*vm.calculateSilver = () => {
			var g = $scope.evaluation.silver;
			if( formValidation.checkErrWhr(['silverType', 'silverWeight']) ){
				vm.silverPrice = vm.silverType[g.silverType] * g.silverWeight;
			} else {
				var err = formValidation.getErrWhr(['silverType', 'silverWeight']);
				formValidation.setErr(err);
			}
		};*/

		/* Pecentage calculator */

		/*vm.calculatePerc = (type) => {

			var cash = (type == 'gold') ? vm.goldPrice : vm.silverPrice;
			var calc;
			var errs = (type == 'gold') ? ['cash', 'percDays'] : ['cashSilver', 'percDaysS'];

			if( formValidation.checkErrWhr(errs) ){
				if( cash < 5000 ){
					calc = cash * 0.066;
				}
				if( cash >= 5000 && cash < 10000 ){
					calc = cash * 0.063;
				}
				if( cash >= 10000 ){
					calc = cash * 0.061;
				}
				
				(type == 'gold') ? (vm.handCasheGold = calc) : (vm.handCasheSilver = calc);
			} else {
				var err = formValidation.getErrWhr(errs);
				formValidation.setErr(err);
			}
		};*/

		/* Tech calculator */

		/*vm.calculateTech = () => {
			if( formValidation.checkErrExpt(['cash', 'percDays', 'goldType', 'goldWeight', 'silverType', 'silverWeight']) ){
				vm.techErrState = true;
			} else {
				let errs = formValidation.getErrWhr(['techType', 'techModel', 'exState', 'telephone', 'email']);
				vm.techErrState = false;
				vm.techErr = errs.map(err => {
					return err.$name;
				});
				formValidation.setErr(errs);
			}
		};*/

		vm.calculateTech2 = () => {
			if( formValidation.checkErrExpt(['cash', 'percDays', 'metalType', 'metalWeight', 'telephone', 'email']) ) {
				vm.techErrState = true;
				vm.searchOffice = true;
				vm.techPrice = $scope.evaluation.techModel.price;
			} else {
				let errs = formValidation.getErrWhr(['techType', 'techModel', 'exState', 'techBrand']);
				vm.techErrState = false;
				vm.techErr = errs.map(err => {
					return err.$name;
				});
				formValidation.setErr(errs);
				vm.searchOffice = false;
			}
		};

		vm.sendInfoReq = () => {
			if( formValidation.checkErrExpt(['cash', 'percDays', 'metalType', 'metalWeight', 'telephone', 'email']) ) {
				vm.techErrState = true;
				vm.searchOffice = true;
				vm.userInfo = false;
			} else {
				let errs = formValidation.getErrWhr(['techType', 'techModel', 'exState', 'telephone', 'email']);
				vm.techErrState = false;
				vm.techErr = errs.map(err => {
					return err.$name;
				});
				formValidation.setErr(errs);
				vm.searchOffice = false;
			}
		};

		vm.getTechTypes = () => {
			return formHttp.getTechTypes().success(data => {
				vm.techTypes = data;
			});
		};
		/*vm.getTechTypes = () => {
			return formHttp.getTechTypes().then(data => {
				vm.techTypes = data;
				vm.techTypes.unshift({id: -1, name: 'Выберите вид'});
				$scope.evaluation.techType = vm.techTypes[0];

				setInputInvalid('techType');
				setInputInvalid('techBrand');
				setInputInvalid('techModel');
			}, err => {
				alert(err.name);
				alert(err.message);
			});
		}*/

		vm.getBrandsByType = type => {
			formHttp.getBrandsByType(type.id).then(data => {
				vm.techBrands = data;
				vm.techBrands.unshift({id: -1, name: 'Выберите бренд'});
				$scope.evaluation.techBrand = vm.techBrands[0];
			}, err => {
				alert(err.name);
				alert(err.message);
			})/*.then(data => {
				//$timeout(() => {
					setInputInvalid('techBrand');
				//}, 1);
				return data;
			});*/
		}

		function setInputInvalid(input) {
			$scope.evalForm[input].$setValidity('required', false);
		}

		vm.getModelsByBrand = brand => {
			formHttp.getModelsByBrand(brand.id).then(data => {
				vm.techModels = data;

				vm.techModels.unshift({id: -1, name: 'Выберите модель', price: 0});
				$scope.evaluation.techModel = vm.techModels[0];
			});
		}
		/* End - Tech calculator */

		/* Modal */
		vm.openModal = ($event, type) => {
			$event.preventDefault();
			var url = 'templates/' +type + '.modal.html';
			var modalInstance = $uibModal.open({
				templateUrl: url,
				controller: ($scope) => {
					$scope.close = () => {
						$uibModalInstance.close();
					};
				}
			});
		}

		$(document).ready(() => {
			console.log(vm.techTypes);
		})

	}

})();