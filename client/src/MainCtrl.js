(function(){
    'use strict';

    angular
        .module('app')
        .controller('MainCtrl', MainCtrl);

    function MainCtrl($log, api){

        var vm = this;

        vm.activeTab   = 1;

        vm.contentBlocks = [];
        vm.sliderImg = [];
        vm.telephoneNumber = '';

        vm.spinnerLoading = false;

        vm.stateModel = false;

        vm.closeMenu = () => {
            vm.stateModel = false;
            vm.menuDropDown.isFirstOpen = false;
            vm.menuDropDown.isSecondOpen = false;
        }
        
        vm.activateTab = $event => {
            let el = $event.target || $event.srcElement;
            removeActiveTab();
            $(el).addClass('active-tab');
        };
        
        function removeActiveTab(){
            $('.nav-tabs a').removeClass('active-tab');
        }

        vm.sliderActiveIdx = 0;

        vm.flickityOptions = {
            initialIndex: 3
        };

        vm.onSlickLoad = () => true;

        vm.menuDropDown = {
            isFirstOpen: false,
            isSecondOpen: false,
            aboutPage: {
                first: false,
                second: false,
                third: false,
                fourth: false
            },
            payCreditsPage: {
                first: false,
                second: false
            },
            techZalogPage: {
                first: false,
                second: false,
                third: false
            },
            nearestOffice: {
                first: false
            },
            officeAddress: {
                first: false,
                search: {
                    first: false,
                    second: false
                }
            }
        }

        setSlides();
        setHomeBlocks();

        function setSlides() {
            return getSlides().then(slides => {
                    // $log.info(slides);
                });
        }

        function getSlides() {
            return api.getSlides()
                .then(data => {
                    vm.sliderImg = data;
                    return vm.sliderImg;
                });
        }

        function setHomeBlocks() {
            return getHomeBlocks().then(blocks => {
                    // $log.info(blocks);
                });
        }

        function getHomeBlocks() {
            return api.getHomeBlocks()
                .then(data => {
                    vm.contentBlocks = data;
                    return vm.contentBlocks;
                });
        }
        
        function setTelNumber() {
            return getTelNumber().then(tel => {
                    // $log.info(tel);
                })
        }
        
        function getTelNumber() {
            return api.getTelephoneNumber()
                .then(data => {
                    vm.telephoneNumber = data;
                    return vm.telephoneNumber;
                })
        }

    }

})();