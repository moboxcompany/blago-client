(() => {
	'use strict';

	angular
		.module('app')
		.controller('FormCtrl', FormCtrl);

	FormCtrl.$inject = ['$scope', '$uibModal', 'formHttp', 'formValidation'];

	function FormCtrl($scope, $uibModal, formHttp, formValidation){

		var vm 		= this;
		var tmpDate = new Date();

		formValidation.setScope($scope);
		formValidation.setForm('questForm');

		// D\M\Y arrays for questForm( anketa )
		vm.dayNumbers   = Array.from({length: 31}, (v, k) => {
			return {
				id: k+1,
				disabled: false
			}
		});
		vm.dayNumbers.unshift({
			id: "День",
			disabled: true
		});
		vm.monthNumbers = Array.from({length: 12}, (v, k) => {
			tmpDate.setMonth(k);
			return {
				id: tmpDate.toDateString().slice(4, 7),
				disabled: false
			}
		});
		vm.monthNumbers.unshift({
			id: "Месяц",
			disabled: true
		});
		vm.yearNumbers  = Array.from({length: 90}, (v, k) => {
			return {
				id: k + 1926,
				disabled: false
			}
		});
		vm.yearNumbers.unshift({
			id: "Год",
			disabled: true
		});

		$scope.user = {};
		$scope.user.data = {};
		$scope.user.data.birthday = {};
		$scope.user.data.birthday.day = vm.dayNumbers[0].id;
		$scope.user.data.birthday.month = vm.monthNumbers[0].id;
		$scope.user.data.birthday.year = vm.yearNumbers[0].id;

		vm.openModal = () => {
			let url = '/templates/anketa.modal.html';
			let modalInstance = $uibModal.open({
				templateUrl: url,
				controller: ModalInstanceCtrl
			});
		}
		
		vm.submitQuest = () => {
			console.log($scope.questForm);
			if( formValidation.checkErr() ){
				formHttp.sendQuestForm($scope.user).then(res => {
					vm.openModal();
				});
			} else {
				formValidation.setErr(formValidation.getErr());
			}
		}

		vm.submitQuiz = () => {
			if( $scope.quizForm.$valid ){
				location.pathname = 'opros_success.html';
			}
		}

		vm.quiz = {
			lombardUsage: {
				yes: false,
				no: false
			},
			whyPickLombard: {
			},
			anotherLombard: {
			},
			advertDesign: {
			},
			desertProgramm: {
			},
			bonusTarget: {
			},
			socialNetworks: {
			}
		};

		vm.checkSelected = function (object) {
	    	return Object.keys(object).some(function (key) {
	    		return object[key];
	    	});
	    }

		vm.checkEmail = () => {
			if( formHttp.checkEmail() ){

			} else {

			}
		}

		function ModalInstanceCtrl($scope, $uibModalInstance, $window){
			$scope.close = () => {
				$uibModalInstance.close();
			};
			$scope.quizRedirect = () => {
				$window.location = '/quiz';
			}
			$scope.homeRedirect = () => {
				$window.location = '/';
			}
		}
	}

})();