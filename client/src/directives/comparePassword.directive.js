(function(){
    'use strict';

    angular
        .module('app')
        .directive('pwCompare', pwCompare);

    function pwCompare(){
    	return {
    		require: 'ngModel',
    		link: function($scope, $element, $attrs, $ctrl){
    			var comparing = '#' + $attrs.pwCompare;
    			$element.on('keyup', function(){
    				$scope.$apply(function(){
    					var v = $element.val() == $(comparing).val();
    					$ctrl.$setValidity('isSimilar', v);
    				});
    			});
    		}
    	}
    }

})();