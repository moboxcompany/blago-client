(() => {

	angular
		.module('app')
		.directive('resizeSlider', resizeSlider);

	resizeSlider.$inject = ['$window', '$document'];

	function resizeSlider($window, $document) {
		return {
			restrict: 'AE',
			link: link
		};

		function link(scope, element, attrs) {
			angular.element($window).bind('resize', sliderResize);

			angular
				.element(document)
				.ready(sliderResize);

			function sliderResize(){
				let slider 	 	= angular.element(element),
					slides 		= document.querySelectorAll('.slide'),
					startHeight = slides[0].height;

				slider.css('height', startHeight + 'px');
			}
		}
	}

})();