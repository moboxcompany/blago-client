(() => {

    angular
        .module('app')
        .controller('GeoCtrl', geoController);

    function geoController(NgMap, NavigatorGeolocation, api, $uibModal, $log) {

        const vm = this;
        // API Key
        // Old: AIzaSyDEkVB4nZR44FqQYuvVo88Xgp6-ASfycSo
        // New: AIzaSyCE2dZf9eZHxMnx1D4BUS8vhgxJtgigfrg
        vm.googleMapsUrl 	= 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCE2dZf9eZHxMnx1D4BUS8vhgxJtgigfrg';
        // Map & Render states
        vm.map;
        vm.mapCompiled = false;
        vm.locDetected = false;
        // Nearest departments in 10 km radius 
        vm.nearestDepartments = [];
        // Nearest dep. obj
        vm.nearest = {};
        // Icon
        vm.markerIcon = {};
        // User's location
        vm.geocoords = {
            lat: 0,
            lng: 0
        };
        // Click marker -> open modal window
        vm.openModal = openModalFn;

        setMap()
            .then(setNearestDep)
            .then(data => {
                vm.locDetected = true;
            })
            .then(setMarkers)
            .then(data => {
                vm.mapCompiled = true;
            });

        function setMap() {
            return getMap()
                .then(getMarkerIconTemplate)
                .then(getGeoCoords)
                .then(data => {

                });
        };

        function getMap() {
            return NgMap.getMap().then(map => {
                    vm.map = map;
                    return vm.map;
                });
        };

        function getMarkerIconTemplate(map) {
            vm.markerIcon = {
                url: 'img/map_icon.png',
                size: new google.maps.Size(91, 55),
                origin: new google.maps.Point(0,0),
                anchor: new google.maps.Point(0,32)
            };
            return vm.markerIcon;
        };

        function getGeoCoords() {
            return NavigatorGeolocation.getCurrentPosition()
                .then(pos => {
                    vm.geocoords.lat = pos.coords.latitude;
                    vm.geocoords.lng = pos.coords.longitude;
                    return vm.geocoords;
                });
        };

        function setMarkers() {
            let myMarkConf  = {
                    position: new google.maps.LatLng(vm.geocoords.lat, vm.geocoords.lng),
                    title: 'Ваше местоположение',
                    zIndex: 5,
                    map: vm.map
                };

            vm.nearestDepartments.forEach(marker => {
                let markConf = {
                    icon: _.clone(vm.markerIcon),
                    position: new google.maps.LatLng(marker.lat, marker.long),
                    title: 'Отделение №' + marker.id,
                    zIndex: 5,
                    map: vm.map
                };

                let mapMark = new google.maps.Marker(markConf);

                mapMark.addListener('click', () => {
                    vm.openModal(marker);
                });
            });
            new google.maps.Marker(myMarkConf);
            return {
                msg: 'markers set up'
            }
        };

        function setNearestDep() {
            return getNearestDepartmentCoords().then(dep => {
                // $log.dep
                return dep;
            });
        };

        function getNearestDepartmentCoords() {
            return api.getNearestDepartment(vm.geocoords)
                .then(departments => {
                    if( departments ) {
                        vm.nearestDepartments = departments.slice(0,4);
                        vm.nearest = vm.nearestDepartments[0];
                        //$log.info(vm.nearest);
                        return vm.nearest;
                    }
                });
        };

        function openModalFn(opt) {
            let modal = $uibModal.open({
                    templateUrl: 'templates/map.modal.html',
                    resolve: {
                        options: () => {
                            return opt;
                        }
                    },
                    controller: ModalInstanceCtrl
                });

            modal.result.then(depId => {
                changeNearestDepartment(depId);
            }, cls => {
                //$log.info('Modal dismissed');
            });
        }

        function ModalInstanceCtrl($scope, $uibModalInstance, options){
            // Mock image, real image - options.image
            let image = 'img/blago_punkt.jpg';
            $scope.image		= '<img ng-src="'+image+'">';
            $scope.text 		= options.address + '<br/>' + options.work + '<br/>' + options.description;
            $scope.officeNumber = options.id;

            $scope.calcPath = () => {
                $uibModalInstance.close($scope.officeNumber);
            };

            $scope.close = () => {
                $uibModalInstance.dismiss({
                    $value: 'cancel'
                });
            };
        };

        function changeNearestDepartment(depId) {
            vm.nearest = _.first(vm.nearestDepartments
                .filter(department => {
                    return department.id === depId;
                }));
        };
    };

})();